#!/usr/bin/env python3

from csv import reader
import configparser
import sys
from influxdb import InfluxDBClient
import argparse
import json
from datetime import datetime


parser = argparse.ArgumentParser()
parser = argparse.ArgumentParser(description='Process some arguments.')
parser.add_argument('--file', type=str, required=True)
args = parser.parse_args()

# Get some variables outside this script
config = configparser.ConfigParser()

try:
    f = open("config.cfg", 'rb')
except OSError:
    print("Could not open/read file: config.cfg")
    sys.exit()

with f:
    config.read("config.cfg")
    csv_file = args.file
    influx_host = config['DETAILS']['INFLUXDB_URL']
    db_name = config['DETAILS']['DB_NAME']
    measurement = config['DETAILS']['MEASUREMENT']


client = InfluxDBClient(host=influx_host, port=8086)
client.create_database(db_name)
client.get_list_database()
client.switch_database(db_name)

# open file in read mode
with open(csv_file, 'r', encoding='latin-1') as read_obj:
    # pass the file object to reader() to get the reader object
    csv_reader = reader(read_obj)
    # Check if csv has header and skip parsing that header is not empty
    header = next(csv_reader)
    if header != None:
        # Iterate over each row in the csv using reader object
        for row in csv_reader:
            # row variable is a list that represents a row in csv
            #print(row)
            #print(type(row[6]))

            # Parse the date in row[4] and reformat it to YYYY-MM-DD
            original_date = row[4]
            try:
                date_obj = datetime.strptime(original_date, '%Y-%m-%d')
            except ValueError:
                # If the date format is DD-MM-YYYY, try parsing it with that format
                date_obj = datetime.strptime(original_date, '%d-%m-%Y')
                # Reformat the date to YYYY-MM-DD
                original_date = date_obj.strftime('%Y-%m-%d')

            bedrag = float(row[6].replace(',','.'))

            #print(row[4], float(row[6].replace(',','.')), row[8], row[9])
            if float(row[6].replace(',','.')) < 0:
                bijaf = "af"
                bedrag_positief = str(bedrag)[1:]
            else:
                bijaf = "bij"
                bedrag_positief = bedrag

            # Handle row[7] being empty or containing whitespace
            saldo_str = row[7].strip()  # Remove leading/trailing whitespace
            if saldo_str:
                saldo = float(saldo_str.replace(',', '.'))
            else:
                saldo = 0.0

            # Ensure that row[9] starts with a capital letter and is not all uppercase
            tegenpartij = row[9].title()

            json_body = [
                {
                    "measurement": measurement,
                    "tags": {
                        "bijaf": bijaf,
                        "volgnr": row[3],
                        "rekening": row[0],
                        "tegenrekening": row[8],
                        "tegenpartij": tegenpartij,
                        "code": row[13],
                        "transactiereferentie": row[15],
                        "machtigingskenmerk": row[16],
                        "omschrijving": row[19]
                    },
                    "time": original_date + "T0:00:00Z",
                    "fields": {
                        "bedrag": bedrag,
                        "bedrag_positief": float(bedrag_positief),
                        "saldo": saldo
                    }
                }
            ]
            jsonStr = json.dumps(json_body)
            print(jsonStr)
            client.write_points(json_body)
