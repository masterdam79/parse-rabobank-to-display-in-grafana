#!/usr/bin/env python3

from csv import reader
import configparser
import mysql.connector
from mysql.connector import Error
import argparse
import json
import sys
from datetime import datetime

parser = argparse.ArgumentParser()
parser = argparse.ArgumentParser(description='Process some arguments.')
parser.add_argument('--file', type=str, required=True)
args = parser.parse_args()

# Get some variables outside this script
config = configparser.ConfigParser()

try:
    f = open("config.cfg", 'rb')
except OSError:
    print("Could not open/read file: config.cfg")
    sys.exit()

with f:
    config.read("config.cfg")
    csv_file = args.file
    mysql_host = config['DETAILS']['MYSQL_HOST']
    mysql_port = config['DETAILS']['MYSQL_PORT']
    db_name = config['DETAILS']['SQL_DB_NAME']
    user = config['DETAILS']['DB_USER']
    password = config['DETAILS']['DB_PASS']
    table = config['DETAILS']['TABLE']

try:
    connection = mysql.connector.connect(host=mysql_host,
                                         port=mysql_port,
                                         database=db_name,
                                         user=user,
                                         password=password)

    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)

        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You're connected to database: ", record)

except Error as e:
    print("Error while connecting to MySQL", e)

# open file in read mode
with open(csv_file, 'r', encoding='latin-1') as read_obj:
    # pass the file object to reader() to get the reader object
    csv_reader = reader(read_obj)
    # Check if csv has header and skip parsing that header is not empty
    header = next(csv_reader)
    if header != None:
        # Iterate over each row in the csv using reader object
        for row in csv_reader:
            # row variable is a list that represents a row in csv
            bedrag = float(row[6].replace(',','.'))
            datetime_str = row[4] + " 00:00:00"
            datetime_obj = datetime.strptime(datetime_str, '%Y-%m-%d %H:%M:%S')

            if float(row[6].replace(',','.')) < 0:
                bijaf = "af"
                bedrag_positief = str(bedrag)[1:]
            else:
                bijaf = "bij"
                bedrag_positief = bedrag

            insert_query = f"""INSERT INTO {table} (bijaf, volgnr, rekening, tegenrekening, tegenpartij, code, transactiereferentie, machtigingskenmerk, omschrijving, time, bedrag, bedrag_positief, saldo)
                              VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) """

            record_to_insert = (bijaf, row[3], row[0], row[8], row[9], row[13], row[15], row[16], row[19], datetime_obj, bedrag, float(bedrag_positief), float(row[7].replace(',','.')))

            cursor.execute(insert_query, record_to_insert)
            connection.commit()

if (connection.is_connected()):
    cursor.close()
    connection.close()
    print("MySQL connection is closed")